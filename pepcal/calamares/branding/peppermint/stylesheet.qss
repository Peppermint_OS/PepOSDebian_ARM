/* A branding component can ship a stylesheet (like this one)
which is applied to parts of the Calamares user-interface.
In principle, all parts can be styled through CSS.
Missing parts should be filed as issues.

The IDs are based on the object names in the C++ code.
You can use the Debug Dialog to find out object names:
  - Open the debug dialog
  - Choose tab *Tools*
  - Click *Widget Tree* button
The list of object names is printed in the log.

Documentation for styling Qt Widgets through a stylesheet
can be found at
    https://doc.qt.io/qt-5/stylesheet-examples.html
    https://doc.qt.io/qt-5/stylesheet-reference.html
In Calamares, styling widget classes is supported (e.g.
using `QComboBox` as a selector).

This example stylesheet has all the actual styling commented out.
The examples are not exhaustive. */

/*########Current Calamres Styles##########*/
/* These "Q's" are master styles for overall colours */
#mainApp {background-color: #333333 }

/* Set the entire Font and Background color */
QWidget { color: #FFFFFF ; background-color: #333333 }
#globalStorageTab { background-color: #545454 }

/* Set Drop Down Font and Background color */
QComboBox { font: 12px ;
	color: #FFFFFF ;
	background-color: #454545 ;
	padding: 2px ;
	border-radius: 10px }
#languageWidget { font: 14px ;
	color: #111111 ;
	background-color: #AAAAAA ;
	text-align: center ;
	border-radius: 6px }

/* Set the inline Text box edit Font and Background color */
QLineEdit { font: 14px ;
	color: #000000 ;
	background-color: beige }

/* Set List box Font and Background color */
QListView { font: 12px ;
	color: #FFFFFF ;
	background-color: #454545 ;
	alternate-background-color: #454545 ;
	padding: 2px ;
	border-radius: 4px }
#layoutSelector {}

/* Set the TreeView Font and Background color */
QTreeView { 
	color: #FFFFFF ;
	background-color: #454545 ;
	border-radius: 12px }

/* Set buttons Font and Background color */
QPushButton { font: 14px ;
	color: #FFFFFF ;
	background-color: #454545 ;
	border-color: #000000 ;
	border-width: 10px ;}
#view-button-back { font: 16px ; background-color: #504545 }
#view-button-back:hover { font: 18px ; color: #ced174 ; background-color: #222222 }
#view-button-next { font: 16px ; background-color: #504545 }
#view-button-next:hover { font: 18px ; color: #00FF00 ; background-color: #222222 }
#view-button-cancel { font: 16px ; background-color: #504545 }
#view-button-cancel:hover { font: 18px ; color: #F62817 ; background-color: #222222 }
#view-button-install { font: 16px ; color: #F6D174 }
#view-button-done { font: 16px ; color: #000000 }

/* Set the color in the keyboard selection */
#layoutSelector { selection-background-color: #F62817 }
#variantSelector { selection-background-color: #F62817 }

/* Set the CheckBox size and Background color */
QCheckBox { 
	color: #000000 ;
	background-color: #545454 ;
	border-width: 2px ;
	border-color: #000000 }
#m_encryptCheckBox { font: 14px ;
	color: #F62817 ;
	background-color: #454545 ;
	border-radius: 4px ;
	padding: 6px }
#checkBoxDoAutoLogin { font: 14px ;
	color: #AAAAAA ;
	background-color: #333333 ;
	padding: 4px }
#restartCheckBox { font: 14px ;
	color: #FFFFFF ;
	background-color: #454545 ;
	border-color: #000000 ;
	border-radius: 8px ;
	height: 16px ;
	width: 100px ;
	padding: 4px }
QCheckBox#restartCheckBox::indicator:checked { image: url(/usr/share/pixmaps/ckm.png) }


/* Set the color when selected */ 
QRadioButton {
	color: #000000 ;
	background-color: #545454 ;
	padding: 4px ;
	border-radius: 4px }
QComboBoxPrivateContainer { background-color: #545454 }
QComboBoxListView { background-color: #545454 }
#qt_scrollarea_hcontainer { background-color: #545454 }
#qt_scrollarea_vcontainer { background-color: #545454 }
QRadioButton::indicator { width: 18px ; height: 18px }
QRadioButton::indicator::checked { image: url(/usr/share/pixmaps/peppermint.png) }
QRadioButton::indicator::checked:hover { image: url(/usr/share/pixmaps/peppermint-fm-20.png) }
QRadioButton::indicator::unchecked:hover { image: url(/usr/share/pixmaps/peppermint-inst.png) }

/* Set the tip Font and Background color */
QToolTip { font: 12px ;
	color: #333333 ;
	background-color: #ced174 ;
	padding: 4px ;
	border-radius: 4px }

/* Set the Progress Bar alignment and load color */
QProgressBar { text-align: center }
QProgressBar::chunk { background-color: #F62817 }

#debugButton  {font: 11px }
#sidebarMenuApp{ background-color: #333333 }
#tabWidget { background-color: #545454 }

/* This area is commented out*/
/* These are for specific controls in specific areas of the installer
QFrame { color: #ced174 ; background-color: #ced174 }
#tabWidget {background-color: #333333;}
#mainApp {background-color: #333333;}
#summaryStep {background-color: #333333;}
#Page_Keyboard {background-color: #333333;}
#debugButton  {font : 11px;}
#debugButton:hover {color : white;}
#checkBoxDoAutoLogin {background-color: #333333; color : white;}
#restartCheckBox {background-color: #333333; color : white;}
#m_selectLabel { background-color: #545454 }
#m_typeLabel { background-color: #545454 }


#sidebarMenuApp{ background-color: #333333;}
#aboutButton{ background-color: #333333; color: white; }
#donateButton{ background-color: #333333; color: #333333; }
#supportButton{ background-color: #333333; color: #333333; }
#knownIssuesButton{ background-color: #333333; color: #333333; }
#releaseNotesButton{ background-color: #333333; color: #333333; }
QStackedWidget { background-color: #545454 }
ChoicePage { background-color: #545454 }
QRadioButton::indicator::unchecked { image: url(/usr/share/pixmaps/ckm.png) }
*/
/* # canary yellow - #ced174 } */
