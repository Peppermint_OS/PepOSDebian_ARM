#!/bin/bash
PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"

# Set the working folder variable
uchinanchu="$(pwd)"


# This cleanup might be better served in the BldHelper*.sh script.
# Create the build folder, move into it removing stale mountpoints and files there.
[ -e fusato ] && [ ! -d fusato ] && rm -f fusato || [ ! -e fusato ] && mkdir fusato
cd fusato

# Within the build, can be layered mounts inside multiple chroots.
umount $(mount | grep "${PWD}/chroot" | tac | cut -f3 -d" ") 2>/dev/null
for i in ./* ./.build ./cache/bootstrap ./cache/contents.chroot ; do [ $i = ./cache ] && continue || rm -rf $i ; done

#exit

# Set of the structure to be used for the ISO and Live system.
# See /usr/lib/live/build/config for a full list of examples.
# Up above is the manual description of what options I used so far.
lb config \
--clean \
--color \
--quiet \
--archive-areas "main contrib non-free" \
--architectures amd64 \
--apt-recommends true \
--backports true \
--binary-images iso-hybrid \
--cache true \
--mode debian \
--distribution bookworm \
--firmware-binary true \
--firmware-chroot true \
--iso-application "PeppermintOS" \
--iso-preparer "PeppermintOS-https://peppermintos.com/" \
--iso-publisher "Peppermint OS Team" \
--iso-volume "PeppermintOS" \
--image-name "PepOS" \
--linux-flavours amd64 \
--security true \
--updates true \
--win32-loader false \
--checksums sha512 \
--zsync false \


# Install the XFCE Desktop
mkdir -p     $uchinanchu/fusato/config/package-lists/
echo xfce4 > $uchinanchu/fusato/config/package-lists/desktop.list.chroot 

# Install software
echo "# Install software to the squashfs for calamares to unpack to the OS.
adwaita-icon-theme 
alsa-utils 
arandr 
arc-theme 
bluez 
bluez-firmware 
calamares-settings-debian 
calamares 
cryptsetup 
cryptsetup-initramfs 
curl 
cups 
dconf-editor 
dkms 
dbus-x11 
efibootmgr 
firmware-linux 
firmware-linux-nonfree 
firmware-misc-nonfree 
firmware-realtek 
firmware-atheros 
firmware-bnx2 
firmware-bnx2x 
firmware-brcm80211 
firmware-intelwimax 
firmware-iwlwifi 
firmware-libertas 
firmware-netxen 
firmware-zd1211 
firmware-ralink 
fonts-cantarell 
fonts-liberation 
gdebi 
gir1.2-webkit2-4.0 
git 
gparted 
gnome-disk-utility 
gnome-system-tools 
grub-pc 
gvfs-backends 
inputattach 
inxi 
locales 
locales-all 
menulibre 
nala
neofetch 
network-manager-gnome 
ntp 
nvidia-detect 
os-prober 
python3-pip 
python3-tk 
python3-bs4 
python3-requests 
python3-ttkthemes 
python3-pyqt5 
python3-pyqt5.qtsvg 
python3-pyqt5.qtwebkit 
python3-pil.imagetk
python3-apt 
screenfetch 
simple-scan 
smbclient 
smartmontools 
sqlite3 
synaptic 
system-config-printer 
mousepad 
xfce4-battery-plugin 
xfce4-clipman-plugin 
xfce4-power-manager 
xfce4-taskmanager 
xfce4-terminal 
xfce4-screenshooter 
xfce4-whiskermenu-plugin 
yad 
wireless-tools 
wget 
f2fs-tools 
xfsprogs 

" > $uchinanchu/fusato/config/package-lists/packages.list.chroot 


# Packages to be stored in /pool but not installed in the OS .
echo "# These packages are available to the installer, for offline use. 
efibootmgr
grub-common
grub2-common
grub-efi
grub-efi-amd64
grub-efi-amd64-bin
grub-efi-amd64-signed
grub-efi-ia32-bin
libefiboot1
libefivar1
mokutil
os-prober
shim-helpers-amd64-signed
shim-signed
shim-signed-common
shim-unsigned

" > $uchinanchu/fusato/config/package-lists/installer.list.binary 


# Setup the chroot structure
mkdir -p $uchinanchu/fusato/config/includes.binary
mkdir -p $uchinanchu/fusato/config/includes.bootstrap/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/local/bin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/applications
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/backgrounds
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/desktop-base
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/icons/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/themes
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/peppermint
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/plymouth
#mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/ice/locale
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/xfce4/helpers
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/fonts/pepconf
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/bin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/sbin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/lib/live/config
#mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/lib/peppermint/ice
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt/sources.list.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/calamares
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/lightdm
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.local/share
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/xdg/xfce4
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pypep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pypep/dbpep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/startpep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pepconf

#mkdir -p $uchinanchu/fusato/config/hooks/live
mkdir -p $uchinanchu/fusato/config/hooks/normal
mkdir -p $uchinanchu/fusato/config/packages.chroot

# Resolves Synaptics issue. Might be better in a conf hook.
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/distro-info
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates
ln -s Debian.info $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates/Peppermint.info
ln -s Debian.mirrors $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates/Peppermint.mirrors
ln -s debian.csv $uchinanchu/fusato/config/includes.chroot/usr/share/distro-info/peppermint.csv


# Copy single files to the chroot
cp $uchinanchu/pepcal/adddesktopicon/add-calamares-desktop-icon $uchinanchu/fusato/config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/calamares/netinstall-* $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/calamares/settings.conf $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/calamares_*_amd64.deb $uchinanchu/fusato/config/packages.chroot
cp $uchinanchu/pepcal/install-peppermint $uchinanchu/fusato/config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/sources-final $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp $uchinanchu/peplightdm/lightdm.conf $uchinanchu/fusato/config/includes.chroot/etc/lightdm
cp $uchinanchu/peplightdm/lightdm-gtk-greeter.conf $uchinanchu/fusato/config/includes.chroot/etc/lightdm
#cp $uchinanchu/../MakePackageLists.sh $uchinanchu/fusato/config/includes.chroot/usr/local/bin
cp $uchinanchu/PepProTools/xDaily $uchinanchu/fusato/config/includes.chroot/usr/local/bin
cp $uchinanchu/PepProTools/Welcome_auto.desktop $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart

# Copy directory contents to the chroot
cp $uchinanchu/pepapplication/*  $uchinanchu/fusato/config/includes.chroot/usr/share/applications
cp $uchinanchu/pepdeffileman/xfce4/* $uchinanchu/fusato/config/includes.chroot/etc/xdg/xfce4
cp $uchinanchu/pepdeffileman/helpers/* $uchinanchu/fusato/config/includes.chroot/usr/share/xfce4/helpers
cp $uchinanchu/pepfont/* $uchinanchu/fusato/config/includes.chroot/usr/share/fonts/pepconf
cp $uchinanchu/pepgrub/* $uchinanchu/fusato/config/includes.chroot/etc/default
cp $uchinanchu/pephooks/live/* $uchinanchu/fusato/config/includes.chroot/usr/lib/live/config
cp $uchinanchu/pephooks/normal/* $uchinanchu/fusato/config/hooks/normal
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.bootstrap/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/usr/lib
cp $uchinanchu/peppackages/* $uchinanchu/fusato/config/packages.chroot
cp $uchinanchu/peppinunstable/* $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
cp $uchinanchu/peppolkit/* $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
cp $uchinanchu/pepsources/* $uchinanchu/fusato/config/includes.chroot/opt
cp $uchinanchu/pepstartpage/* $uchinanchu/fusato/config/includes.chroot/opt/startpep
cp $uchinanchu/pepdb/* $uchinanchu/fusato/config/includes.chroot/opt/pypep/dbpep
cp $uchinanchu/pepuserconfig/* $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
cp $uchinanchu/pepwallpaper/* $uchinanchu/fusato/config/includes.chroot/usr/share/backgrounds
cp $uchinanchu/PepProPixMaps/* $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps
cp $uchinanchu/PepProTools/* $uchinanchu/fusato/config/includes.chroot/opt/pypep

# Copy recursive files and sub-directories, containing symlinks.
cp -dr $uchinanchu/pepicons/Numix $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Pepirus $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Pepirus-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/peptheme/Arc-Red $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Green $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Blue $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Green-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Teal-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Red-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Peppermint-10-Red-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes

cp -r $uchinanchu/pepcal/calamares/branding $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepcal/calamares/modules $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepdesktopbase/desktop-base $uchinanchu/fusato/config/includes.chroot/usr/share/
cp -r $uchinanchu/peploadersplash/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/isolinux  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/pepmenu/menus $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepnemo/nemo $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepplymouth/plymouth $uchinanchu/fusato/config/includes.chroot/usr/share/
cp -r $uchinanchu/pepxfce/xfce4 $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepxfce/Thunar $uchinanchu/fusato/config/includes.chroot/etc/skel/.config


# Place files unique to Testing builds here.
cp    $uchinanchu/peptesting/pepaliases/bash_aliases  $uchinanchu/fusato/config/includes.chroot/etc/skel/.bash_aliases
cp    $uchinanchu/peptesting/pepcal/install-peppermint $uchinanchu/fusato/config/includes.chroot/usr/bin
cp    $uchinanchu/peptesting/pepcal/calamares/settings.conf $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp    $uchinanchu/peptesting/pepcal/sources-final $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp    $uchinanchu/peptesting/pepcal/calamares_*_amd64.deb $uchinanchu/fusato/config/packages.chroot
#cp    $uchinanchu/peptesting/peppackages/ice_6.0.6_all.deb $uchinanchu/fusato/config/packages.chroot
cp    $uchinanchu/peptesting/peppinunstable/* $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
cp -r $uchinanchu/peptesting/pepcal/calamares/modules $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/peptesting/peploadersplash/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peptesting/peploadersplash/isolinux  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peptesting/pepxfce/xfce4 $uchinanchu/fusato/config/includes.chroot/etc/skel/.config

# Build the ISO #
lb build  #--debug --verbose 

