"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this script is for performing utility functions
*
"""

import apt

#gets all installed packages
cache = apt.Cache()

#Checks if a package is installed
def install_check(appname):
    """This one check is a application is installed"""
    try:
        print(appname,cache[appname].is_installed)
        return cache[appname].is_installed
    except:
        #probably not installed or otherwise throws errors on check
        return False
