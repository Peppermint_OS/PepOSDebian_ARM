#!/usr/bin/env python3

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
import sys

class MainWindow(QMainWindow):
#set up the main window and the url address. 
    def __init__(self, *args, **kwargs):
        super(MainWindow,self).__init__(*args, **kwargs)
        self.setWindowTitle("Peppermint Kumo Tutorial")
        self.setWindowIcon(QIcon('/usr/share/pixmaps/peppermint-old.png'))  
        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl("https://peppermint_os.codeberg.page/html/#kumo"))
        self.setCentralWidget(self.browser)
        
        #add a nav toolbar
        self.navigation_bar = QToolBar('Navigation Toolbar')
        self.addToolBar(self.navigation_bar)
        
        #put a back button
        back_button = QAction("Back", self)
        back_button.setStatusTip('Go to previous page you visited')
        back_button.triggered.connect(self.browser.back)
        self.navigation_bar.addAction(back_button)
        
        #put a next button
        next_button = QAction("Next", self)
        next_button.setStatusTip('Go to next page')
        next_button.triggered.connect(self.browser.forward)
        self.navigation_bar.addAction(next_button)        
        
        #put a refresh button
        refresh_button = QAction("Refresh", self)
        refresh_button.setStatusTip('Refresh this page')
        refresh_button.triggered.connect(self.browser.reload)
        self.navigation_bar.addAction(refresh_button)
        self.show()

app = QApplication(sys.argv)
window = MainWindow()

app.exec_()
