#!/bin/bash

pdir=/opt/tmprpox
fdir=/opt/tmprpox/PepProTools
desdir=/opt/pypep
cbr="https://codeberg.org/Peppermint_OS/PepProTools.git"

fwt(){
	###Function to run if the folder was there (fwt)###
	#remove the tmpropox
	rm -r "$pdir"
	#make a new folder tmprox
	mkdir "$pdir"
	#go to the tmprpox folder
	cd "$pdir"
	#clone the protools repo
	git clone "$cbr"	
	#use rsynch to copy files over to the opt location
	#rsync -av /opt/tmprpox/* /opt/pypep
	rsync -av "$fdir"/* "$desdir"
	#Clean up and remove the tmpropox
	rm -r "$pdir"
	
}
fwnt(){
	###Function to run if the folder was not there (fwnt)###
	mkdir "$pdir"
	#go to the tmprpox folder
	cd "$pdir"
	#clone the protools repo
	git clone "$cbr"
	#use rsynch to copy files over to the opt location
	#rsync -av /opt/tmprpox/* /opt/pypep
	rsync -av "$fdir"/* "$desdir"
	#Clean up and remove the tmpropox
	rm -r "$pdir"	
}

fupug(){
	#Run the apt update and upgrade to update from the debian repos
	apt update && apt upgrade -y
}
