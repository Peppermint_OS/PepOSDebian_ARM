"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this files should be used for the over all style and design of
* the Welcome Screen
"""
from tkinter import PhotoImage
import ttkbootstrap as ttk

# This will set the style to used for boostrap
# just change the name to what is needed for the
# the system
bbstyle = ttk.Window(themename="darkly")

# set the title of the window
DEBIAN_TITLE = "Welcome to Peppermint - (Debian)"
DEVUAN_TITLE = "Welcome to Peppermint - (Devuan)"

# set the logo for the screen
debcandy = PhotoImage(file='/opt/pypep/pep-logo-deb.png')
devcandy = PhotoImage(file='/opt/pypep/pep-logo-dev.png')

# add the logo text
peppertxt = PhotoImage(file='/opt/pypep/peppermint-word-white.png')

# set the icons that are used for the community buttons
tms = PhotoImage(file="/opt/pypep/mn.png")
fm = PhotoImage(file="/opt/pypep/sf.png")
mat = PhotoImage(file="/opt/pypep/mt.png")
rd = PhotoImage(file="/opt/pypep/rd.png")
cb = PhotoImage(file="/opt/pypep/cb.png")

# set the message for the community section
MESSAGE_TEXT = ("Come join the conversation and discussion about"
                " PeppermintOS using these links to participate in"
                " the community.")