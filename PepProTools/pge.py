"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This script is used to download extras
"""

import subprocess as sp
import tkinter as tk
import ttkbootstrap as ttk
from tendo import singleton

#setting up window
pex = ttk.Window(themename="darkly")
WINDOW_HEIGHT = 230
WINDOW_WIDTH = 355
pex.title('Get Extras')
pex.tk.call('wm', 'iconphoto', pex._w, tk.PhotoImage(file='/usr/share/pixmaps/peppermint-old.png'))


#keep only instance at a time
me = singleton.SingleInstance()

#main title style
spmt = ttk.Style()
spmt.configure("#F62817.TLabel", foreground ="#b12026",
                background ="#222222", font = ("Helvetica", '12', 'bold') )

###Verbiage and titel Placements###

lblpmwelttl = ttk.Label(pex, style="Whiteb.TLabel",
                        text="Download and Install Wallpaper, Themes and Icons ")
lblpmwelttl.place(x=10, y=10)
#Functions to open the other pythons files. or close this screen ###
#open the sure scripts
def icons():
    """get the icons"""
    pex.destroy()
    sp.call(['xfce4-terminal', '-x','/opt/pypep/icons.sh'])


def themes():
    """get the themes"""
    pex.destroy()
    sp.call(['xfce4-terminal', '-x','/opt/pypep/themes.sh', '&'])


def wall_papers():
    """get the wallpapers"""
    pex.destroy()
    sp.call(['xfce4-terminal', '-x','/opt/pypep/walls.sh', '&'])

def quit_pge():
    """quit the window"""
    pex.destroy()

def center_screen():
    """ gets the coordinates of the center of the screen """
    screen_width = pex.winfo_screenwidth()
    screen_height = pex.winfo_screenheight()
    # Coordinates of the upper left corner of the window to make the window
    # appear in the center
    x_cordinate = int((screen_width / 2) - (WINDOW_WIDTH / 2))
    y_cordinate = int((screen_height / 2) - (WINDOW_HEIGHT / 2))
    pex.geometry("{}x{}+{}+{}".format(WINDOW_WIDTH,
                   WINDOW_HEIGHT, x_cordinate, y_cordinate))

###All the buttons and placements##
themes_button = ttk.Button(text="Peppermint Themes",
          bootstyle="danger-outline", command=themes,  width=20)
themes_button.place(x=90, y=50)

icons_button = ttk.Button(text='Peppermint Icons', command=icons,
        bootstyle="danger-outline", width=20 )
icons_button.place(x=90, y=90)

wall_papers_button = ttk.Button(text='Peppermint WallPaper', command=wall_papers,
        bootstyle="danger-outline", width=20 )
wall_papers_button.place(x=90, y=130)

close_button = ttk.Button(text="Close", bootstyle="light-outline",
        command=quit_pge,  width=10)
close_button.place(x=225, y=190)

#call Center screen
center_screen()
pex.mainloop()
